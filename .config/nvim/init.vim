"Plug-ins
call plug#begin('~/.config/nvim/plugged')
" Tools
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'preservim/nerdtree'
" Syntax
Plug 'ap/vim-css-color' "Displays a preview of colors with CSS 
Plug 'tpope/vim-surround'
Plug 'lervag/vimtex' "LaTeX syntax
Plug 'sirver/ultisnips' "LaTeX snippets
" Color-schemes
Plug 'arcticicestudio/nord-vim'
call plug#end() 

" set the Leader key
nnoremap <Space> <Nop>
let mapleader=" "

"General Settings
filetype plugin indent on  "Enabling Plugin & Indent
syntax on  "Turning Syntax on
syntax enable
set spell spelllang=it,en,cjk
set clipboard+=unnamedplus "Enable copy to clipboard with yank
set number relativenumber "Setting line number
set visualbell
set encoding=UTF-8
set wrap
set wildmenu
set shiftwidth=4 smartindent tabstop=4 softtabstop=4 expandtab  
set noswapfile
set nobackup
set hidden
set laststatus=2 cmdheight=1
set splitbelow splitright 
set scrolloff=3 "Cursor motion
set backspace=indent,eol,start
set matchpairs+=<:> " use % to jump between pairs
set incsearch
set ignorecase " case insensitive search
set smartcase
set showmatch
set path+=**
set history=100
set nohlsearch " no highlight on search
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o "Disables automatic commenting on newline

" Key bindings
" Navigation
nnoremap k gk
nnoremap j gj
nnoremap <leader>h <C-W>h
nnoremap <leader>j <C-W>j
nnoremap <leader>k <C-W>k
nnoremap <leader>l <C-W>l

" Files
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader><ENTER> :Goyo<CR>
nnoremap <leader>, :vsplit ~/.config/nvim/init.vim<CR>
nnoremap <C-s> :source ~/.config/nvim/init.vim<CR>

" Resize
nnoremap <Up> :resize +2<CR> 
nnoremap <Down> :resize -2<CR>
nnoremap <Left> :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>

" Shortcut
nnoremap <leader>f :find 
nnoremap <leader>b :b 
nnoremap <leader>i gg=G<CR>
nnoremap Y y$
" Pydoc shortcut
nnoremap <leader>py :<C-u>execute "!pydoc3 " . expand("<cword>")<CR>

" Move lines
xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv

" Find text 
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" VimTex
nmap <leader>v <plug>(vimtex-view)
nmap <leader>c <plug>(vimtex-compile)

" NERDTree always shows hidden files
let NERDTreeShowHidden=1

" LaTeX settings
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'
" Spell check on the fly 
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

" LaTeX snippets settings
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
let g:UltiSnipsSnippetDirectories=[$HOME.'/.config/nvim/snips']


"Color Settings
set nocompatible
if (has("termguicolors"))
  set termguicolors
endif
colorscheme nord
set background=dark

" Otherwise vimtex would highlight concealed text
hi! clear Conceal

let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'

"Goyo settings
function! s:goyo_enter()
    set noshowmode
    set noshowcmd
    Limelight
endfunction

function! s:goyo_leave()
    set showmode
    set showcmd
    Limelight!
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

"Status-line
set statusline=
set statusline+=%#DiffBuff#
set statusline+=\ %M
set statusline+=\ %y
set statusline+=\ %r
set statusline+=\ %F
set statusline+=%= "Right side settings
set statusline+=%#DiffAdd#
set statusline+=\ %c:%l/%L
set statusline+=\ %p%%
set statusline+=\ [%n]
